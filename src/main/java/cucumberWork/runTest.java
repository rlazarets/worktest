package cucumberWork;


import cucumber.api.cli.Main;
import org.apache.commons.lang3.ArrayUtils;


public class runTest {

    public static void main(String[] args) throws Throwable {
        String[] defaults = {
                "--glue", "/Users/lazarets/WorkTest/src/main/resources/features",
                "--plugin", "junit:target/wt/cucumber.xml",
                "--tags", "@wtf",
                "--glue", "cucumberWork",
                "classpath:features",
                "--strict"
        };
        byte exitStatus = Main.run(ArrayUtils.addAll(defaults, args),
                Thread.currentThread().getContextClassLoader());
        System.exit(exitStatus);
    }
}