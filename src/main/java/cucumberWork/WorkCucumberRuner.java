package cucumberWork;

import cucumber.api.junit.Cucumber;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.model.InitializationError;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class WorkCucumberRuner extends Cucumber {

    private static final Set<Object> THREADS = Collections.synchronizedSet(new HashSet<Object>());

    public WorkCucumberRuner(Class clazz) throws InitializationError {
        super(clazz);
    }

    @Override
    public void run(RunNotifier notifier) {
        Thread thread = Thread.currentThread();
        synchronized (THREADS) {
            THREADS.add(thread);
        }
        super.run(notifier);
        synchronized (THREADS) {
            THREADS.remove(thread);
        }
    }
}