package cucumberWork;

import junit.framework.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import cucumber.api.java.en.*;
//import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
//import java.net.URL;


public class CommonStepDefinitions {
    private String webpage = "https://www.rocketmiles.com/";
    Dimension d1 = new Dimension(450, 365);

    WebDriver driver = null;
    //To use remoute Web driver need run docker $ docker run -d -p 8910:8910 wernight/phantomjs phantomjs --webdriver=8910
    //WebDriver driver = new RemoteWebDriver(
    // new URL("http://127.0.0.1:8910"),
    // DesiredCapabilities.phantomjs());

    public CommonStepDefinitions() throws MalformedURLException {
    }


    @Given("^I have open the browser$")
    public void openBrowser() {
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
    }

    @When("^I open Rocketmiles website$")
    public void goToRocketmiles() throws InterruptedException {
        driver.navigate().to(webpage);
        Thread.sleep(10000);
    }

    @Then("^Sign in or Join  element should exits$")
    public void loginButton() {
        if (driver.findElement(By.xpath("//a[contains(text(),'Sign In or Join')]")).isEnabled()) {
            System.out.println("Test 1 Pass");
        } else {
            System.out.println("Test 1 Fail");
        }
        driver.close();
    }

    @Then("Login popUp appeared")
    public void loginPopUpAppeared() throws InterruptedException {
        driver.findElement(By.xpath("//a[contains(text(),'Sign In or Join')]")).click();
        Thread.sleep(5000);
        if (driver.findElement(By.xpath("//div[@class='modal-content']")).isEnabled()) {
            System.out.println("Test 2 Pass");
        } else {
            System.out.println("Test 2 Fail");
        }
    }

    @And("With correct size")
    public void inCorrectLocation() {
        Assert.assertEquals(driver.findElement(By.xpath("//div[@class='modal-content']")).getSize(), d1);
    }

    @Then("All elements exist in Login popup")
    public void allElementsExistInLoginPopup() {
        driver.findElement(By.xpath("//span[@class='ng-binding']")).isEnabled();
        driver.findElement(By.xpath("//button[@class='btn btn-default ng-binding']")).isEnabled();
        driver.findElement(By.xpath("//span[contains(text(),'×')]")).isEnabled();
        driver.findElement(By.xpath("//input[@placeholder='Your Email Address']")).isEnabled();
        driver.findElement(By.xpath("//input[@placeholder='Password']")).isEnabled();
        driver.findElement(By.xpath("//input[@name='rememberMe']")).isEnabled();
        driver.findElement(By.xpath("//label[contains(text(),'Remember Me')]")).isEnabled();
        driver.findElement(By.xpath("//input[@class='col-xs-12 rm-btn-orange login-button rm-animate-fade']")).isEnabled();
        driver.findElement(By.xpath("//button[@class='col-xs-12 rm-btn-facebook facebookLoginLink rm-animate-fade ng-binding ng-scope']")).isEnabled();
        driver.findElement(By.xpath(" //a[contains(text(),'Forgot or Need Password?')]")).isEnabled();
        driver.close();
    }

    @When("I fill in username with {string}")
    public void iFillInWith(String name) {
        driver.findElement(By.xpath("//a[contains(text(),'Sign In or Join')]")).click();
        WebElement username = driver.findElement(By.name("username"));
        username.sendKeys(name);
    }

    @And("I fill in Password with {string}")
    public void iFillInPasswordWith(String psw) {
        WebElement password = driver.findElement(By.name("password"));
        password.sendKeys(psw);
    }

    @And("I click on the Log In button")
    public void iClickOnTheButton() throws InterruptedException {
        WebElement login = driver.findElement(By.xpath("//input[@class='col-xs-12 rm-btn-orange login-button rm-animate-fade']"));
        login.click();
        Thread.sleep(5000);
    }

    @Then("I am on the Profile & Account page on URL {string}")
    public void iAmOnThePageOnURL(String arg0) {
        driver.findElement(By.xpath("//span[@class='hidden-xs']")).click();
        String actualUrl = "https://www.rocketmiles.com/user/account";
        String expectedUrl = driver.getCurrentUrl();
        Assert.assertEquals(expectedUrl, actualUrl);
    }

    @And("I should see Profile & Account message")
    public void iShouldSeeMessage() {
        System.out.println(driver.findElement(By.xpath("//h3[contains(text(),'Profile & Account')]")).isDisplayed());
        driver.close();
    }

    @And("I should see {string} message")
    public void iShouldSeeErrorMessage(String arg0) {
        if (driver.getPageSource().contains(arg0))
            System.out.println("error text present");
        else
            System.out.println("error text not present");

        driver.close();

    }


}