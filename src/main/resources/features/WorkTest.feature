@wtf
Feature: CucumberJava

  Background: User navigates to Company home page
    Given I have open the browser
    Then I open Rocketmiles website

  @wts
  Scenario: Check if login element exists
    Then Sign in or Join  element should exits

  @wts
  Scenario: Check Login popup element exist
    Given Login popUp appeared
    And With correct size
    Then All elements exist in Login popup
  @wts
  Scenario: Successful login
    When I fill in username with "ivanivanov@gmail.com"
    And I fill in Password with "123456789"
    And I click on the Log In button
    Then I am on the Profile & Account page on URL "https://www.rocketmiles.com/user/account"
    And I should see Profile & Account message
  @wts
  Scenario Outline: Failed login using wrong credentials
    When I fill in username with "<username>"
    And I fill in Password with "<password>"
    And I click on the Log In button
    And I should see "<warning>" message
    Examples:
      | username             | password | warning                                                       |
      |                      |          | We were not able to find a user with that email and password. |
      | ivanivanov@gmail.com | 123      | We were not able to find a user with that email and password. |
      #| Tset@.gmail | !23      | Please enter an email address.                                |
      #| Test        |          | Please enter an email address.                                |
      |                      | !123     | We were not able to find a user with that email and password. |